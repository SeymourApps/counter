#include <windows.h>
#include <sstream>

#define PlusBTN 101
#define MinusBTN 102

WNDCLASSEX window;

HWND wHwnd;

HWND number;
HWND name;
HWND plusBtn;
HWND minusBtn;

int current;

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    ZeroMemory(&window, sizeof(WNDCLASSEX));

    window.cbSize = sizeof(WNDCLASSEX);
    window.style = CS_HREDRAW | CS_VREDRAW;
    window.lpfnWndProc = WindowProc;
    window.hInstance = hInstance;
    window.hCursor = LoadCursor(NULL, IDC_ARROW);
    window.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
    window.lpszClassName = "MainWindow";

    RegisterClassEx(&window);

    wHwnd = CreateWindowEx(NULL,
                           "MainWindow", "",
                           WS_OVERLAPPEDWINDOW,
                           300, 300,
                           200, 200,
                           NULL, NULL,
                           hInstance, NULL);
    ShowWindow(wHwnd, nCmdShow);

    MSG msg;
    while(GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch(message) {
        case WM_CREATE: {
            minusBtn = CreateWindow("button", "-1",
                                    WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                    10, 115,
                                    80, 30,
                                    hWnd, (HMENU)MinusBTN, NULL, NULL);
            plusBtn = CreateWindow("button", "+1",
                                   WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                   95, 115,
                                   80, 30,
                                   hWnd, (HMENU)PlusBTN, NULL, NULL);
            name = CreateWindow("edit", "Name",
                                WS_VISIBLE | WS_CHILD | WS_TABSTOP,
                                10, 10,
                                160, 20,
                                hWnd, NULL, NULL, NULL);
            number = CreateWindow("static", "0",
                                  WS_VISIBLE | WS_CHILD,
                                  10, 40,
                                  160, 55,
                                  hWnd, NULL, NULL, NULL);
        } break;
        case WM_COMMAND: {
            switch(LOWORD(wParam)) {
                case PlusBTN: {
                    ++current;
                    std::stringstream v;
                    v << current;
                    SetWindowText(number, v.str().c_str());
                } break;
                case MinusBTN: {
                    --current;
                    std::stringstream v;
                    v << current;
                    SetWindowText(number, v.str().c_str());
                } break;
            }
        } break;
        case WM_DESTROY: {
            PostQuitMessage(0);
            return 0;
        } break;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}
